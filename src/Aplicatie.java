import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Aplicatie extends JFrame {
    JTextField text;
    JButton but;

    Aplicatie() {
        setTitle("Ex2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300, 150);
        setVisible(true);

    }

    public void init() {
        this.setLayout(null);
        int width = 80;
        int height = 20;

        text = new JTextField();
        text.setBounds(10, 20, 200, height);

        add(text);

        but = new JButton("Scrie");
        but.setBounds(10, 50, width, height);

        add(but);

        but.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String s = text.getText();
                System.out.println(s);


            }
        });


    }

    public static void main(String[] args) {
        new Aplicatie();
    }


}
